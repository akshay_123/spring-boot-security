package com.akshay.springboot.security.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.akshay.springboot.security.model.User;

@Service
public class UserService {

	private List<User> userList=new ArrayList<User>();
	public UserService() {
		this.userList.addAll(List.of(
				new User(1L,"Akshay","Akshay@123","Akshay@gmail.com","Admin"),
				new User(2L,"Ram","Ram@123","Ram@gmail.com","Normal")
				));
	}
	
	public List<User> getAllUser(){
		return this.userList;
	}
	
	public User LoadUserByUsername(String username) {
		return this.userList.stream().filter(
				i->i.getUsername().equalsIgnoreCase(username))
				.findAny()
				.orElse(null);
	}
	
	public User saveAUser(User user) {
		this.userList.add(user);
		return user;
	}
}
