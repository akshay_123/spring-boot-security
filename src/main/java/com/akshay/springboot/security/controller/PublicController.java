package com.akshay.springboot.security.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicController {
	
	@GetMapping("/public")
	@PreAuthorize(value = "hasRole('ADMIN')")
	public String publicm(){
		return "Hi";
		
	}

}
