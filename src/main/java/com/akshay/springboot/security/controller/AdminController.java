package com.akshay.springboot.security.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akshay.springboot.security.model.User;
import com.akshay.springboot.security.service.UserService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private UserService userService;
	
	@GetMapping("/")
	public List<User> gellAllUser(){
		return this.userService.getAllUser();
	}
	
	@GetMapping("/{username}")
	public User gellAllUser(@PathVariable ("username") String username){
		return this.userService.LoadUserByUsername(username);
	}
	
	@PostMapping("/")
	public User saveUser(@RequestBody User user){
		return this.userService.saveAUser(user);
	}
	

}
