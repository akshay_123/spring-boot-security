package com.akshay.springboot.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@GetMapping("/")
	public String hompage() {
		return "<h1>This is Home page</h1>";
	}
	
	@GetMapping("/login")
	public String login() {
		return "<h1>This is login page</h1>";
	}
	
	@PostMapping("/registration")
	public String registration() {
		return "<h1>This is registration page</h1>";
	}

}
