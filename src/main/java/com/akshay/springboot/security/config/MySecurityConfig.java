package com.akshay.springboot.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MySecurityConfig {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	///List of URL need to be whitelisted
	private static final String[] WHITE_LIST_URLS = {
            "/hello",
            "/register",
            "/verifyRegistration*",
            "/resendVerifyToken*"
    };
	
	//It is used to Authorize user based on role
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http
         .cors()
         .and()
         .csrf()
         .disable()
				.authorizeRequests()
				.antMatchers("/user/**").hasRole("USER")//user with role "USER" can access to resouce user
				.antMatchers("/admin/**").hasRole("ADMIN")//user with role "ADMIN" can access to resouce admin only
				.and()
				.formLogin();
		return http.build();

	}

	//It is used to encrypt the password
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(11);
	}

	//It is used to authincate the user
//	@Bean
//	public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
//		List<UserDetails> userDetailsList = new ArrayList<>();
//		userDetailsList.add(User.withUsername("employee").password(this.passwordEncoder().encode("password"))
//				.roles("USER").build());
//		userDetailsList.add(User.withUsername("manager").password(this.passwordEncoder().encode("password"))
//				.roles("ADMIN").build());
//
//		userDetailsList.
//		return new InMemoryUserDetailsManager(userDetailsList);
//	}
	
	//user from Mysql DB 
	@Bean
    AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider
                 = new DaoAuthenticationProvider();
        provider.setUserDetailsService(customUserDetailsService);
        provider.setPasswordEncoder(this.passwordEncoder());
        return  provider;
    }

}
